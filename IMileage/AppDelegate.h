//
//  AppDelegate.h
//  IMileage
//
//  Created by Rajith Vithanage on 2/18/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

