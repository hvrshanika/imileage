//
//  DBManager.h
//  IMileage
//
//  Created by Rajith Vithanage on 3/6/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject

+(DBManager*)getSharedInstance;
-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

@property (nonatomic, strong) NSMutableArray *arrColumnNames;
@property (nonatomic,assign) int affectedRows;
@property (nonatomic,assign) long lastInsertedRowID;

-(NSArray *)loadDataFromDB:(NSString *)query;
-(void)executeQuery:(NSString *)query;

@end
