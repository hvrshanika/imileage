//
//  LoginController.h
//  IMileage
//
//  Created by Rajith Vithanage on 3/8/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "DBManager.h"

@protocol LoginUIDelagate;

@interface LoginController : NSObject {
    DBManager *dbManager;
}

@property(nonatomic,weak) id<LoginUIDelagate> uiLoginDelagate;

-(void)registerAccount:(User*)user;
-(void)updateAccount:(User *)user;
-(void)processLogin:(NSString*)email password:(NSString*)password;
-(User*)getUserForId:(int)userId;

@end

@protocol LoginUIDelagate <NSObject>

@optional
-(void)registrationSuccess;
@optional
-(void)registrationFailure:(NSString*)msg;
@optional
-(void)updateSuccess;
@optional
-(void)updateFailure:(NSString*)msg;
@optional
-(void)loginSuccess;
@optional
-(void)loginFailure:(NSString*)msg;
@end
