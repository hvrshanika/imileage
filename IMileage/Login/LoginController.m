//
//  LoginController.m
//  IMileage
//
//  Created by Rajith Vithanage on 3/8/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "LoginController.h"
#import "SettingsDataStore.h"

@implementation LoginController

@synthesize uiLoginDelagate;

-(id) init{
    self = [super init];
    dbManager = [DBManager getSharedInstance];
    return self;
}

-(void)registerAccount:(User *)user{
    NSArray *existing = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT user_email FROM user WHERE user_email = '%@'", user.email]];
    if([existing count] == 0){
        NSString *query = [NSString stringWithFormat:@"INSERT INTO user(user_id, user_name, user_email, password, selected_car_id, tracking_mode) VALUES(NULL,'%@','%@','%@',0,0)",user.name,user.email,user.password];
        
        [dbManager executeQuery:query];
        
        if (dbManager.affectedRows != 0) {
            user.userId = (int)dbManager.lastInsertedRowID;
            [SettingsDataStore getSharedInstance].user = user;
            [uiLoginDelagate registrationSuccess];
        }
        else{
            [uiLoginDelagate registrationFailure:@"Registration Failed"];
        }
    }
    else
        [uiLoginDelagate registrationFailure:@"Email Address already exists"];
}

-(void)updateAccount:(User *)user{
    NSString *query = [NSString stringWithFormat:@"UPDATE user SET user_name = '%@', selected_car_id = %d, tracking_mode = %d WHERE user_id = %d",user.name,user.selectedCarId,user.trackingType,user.userId];
    
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
        [SettingsDataStore getSharedInstance].user = user;
        [uiLoginDelagate updateSuccess];
    }
    else{
        [uiLoginDelagate updateFailure:@"Update Failed"];
    }
}

-(void)processLogin:(NSString*)email password:(NSString*)password{
    NSArray *user = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT user_id, user_name, user_email, selected_car_id, tracking_mode FROM user WHERE user_email = '%@' AND password = '%@'", email,password]];
    if([user count] > 0){
        NSArray *data = user[0];
        User *user = [User alloc];
        user.userId = [(NSString*)data[0] intValue];
        user.name = (NSString*)data[1];
        user.email = (NSString*)data[2];
        user.selectedCarId = [(NSString*)data[3] intValue];
        user.trackingType = [(NSString*)data[4] intValue];
        
        [SettingsDataStore getSharedInstance].user = user;
        
        if(user.selectedCarId > 0){
            NSArray *car = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT car_id, car_name, mileage FROM car WHERE car_id = %d", user.selectedCarId]];
            
            if([car count] > 0){
                NSArray *data = car[0];
                Car *car = [Car alloc];
                car.carId = [(NSString*)data[0] intValue];
                car.carName = (NSString*)data[1];
                car.mileage = [(NSString*)data[2] doubleValue];

                [SettingsDataStore getSharedInstance].car = car;
            }
        }
        [uiLoginDelagate loginSuccess];
    }
    else
        [uiLoginDelagate loginFailure:@"Username or Password Incorrect"];
        
}

-(User*)getUserForId:(int)userId{
    User *user;
    NSArray *userArr = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT user_id, user_name, user_email, selected_car_id, tracking_mode FROM user WHERE user_id = %d", userId]];
    if([userArr count] > 0){
        NSArray *data = userArr[0];
        user = [User alloc];
        user.userId = [(NSString*)data[0] intValue];
        user.name = (NSString*)data[1];
        user.email = (NSString*)data[2];
        user.selectedCarId = [(NSString*)data[3] intValue];
        user.trackingType = [(NSString*)data[4] intValue];
    }
    return user;
}

@end
