//
//  LoginViewController.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/19/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "LoginViewController.h"
#import "MainVC.h"
#import "LoginController.h"
#import "SettingsDataStore.h"
#import "SharedMethods.h"
#import "SettingsController.h"

@interface LoginViewController() <LoginUIDelagate> {
    __weak IBOutlet UITextField *txtUsername;
    __weak IBOutlet UITextField *txtPassword;
    __weak IBOutlet UIButton *btnLogin;
    __weak IBOutlet UIButton *btnForgotPassword;
    __weak IBOutlet UIButton *btnCreateAcc;
    
    LoginController *loginController;
}

@end

@implementation LoginViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    loginController = [[LoginController alloc] init];
    loginController.uiLoginDelagate = self;
}

-(IBAction)loginBtnTapped:(id)sender{
#ifdef DEBUG_AUTO_LOGIN
    if(txtUsername.text == nil || [txtUsername.text isEqualToString:@""]){
        txtUsername.text = DEBUG_AUTO_LOGIN_USERNAME;
        txtPassword.text = DEBUG_AUTO_LOGIN_PASSWORD;
    }
#endif
    
    NSString *error;
    if([txtUsername.text isEqualToString:@""])
        error = @"Please enter Email Address";
    else if([txtPassword.text isEqualToString:@""])
        error = @"Please enter Password";
    
    if(error){
        [SharedMethods showMessage:@"Error" message:error onVC:self];
        return;
    }
    else{
        [loginController processLogin:txtUsername.text password:txtPassword.text];
    }
}

-(void)loginSuccess{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:[SettingsDataStore getSharedInstance].user.userId] forKey:@"userId"];
    [defaults synchronize];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MainVC *vc = [sb instantiateViewControllerWithIdentifier:@"MainVC"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)loginFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

#pragma mark - Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == txtUsername){
        [txtPassword becomeFirstResponder];
        return YES;
    } else {
        [self.view endEditing:YES];
        [self loginBtnTapped:nil];
        return NO;
    }
}


@end
