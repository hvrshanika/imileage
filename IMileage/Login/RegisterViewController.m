//
//  RegisterViewController.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/19/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginController.h"
#import "Car.h"
#import "SettingsController.h"
#import "SettingsDataStore.h"
#import "MainVC.h"
#import "SharedMethods.h"

#pragma mark - RegisterViewController

@interface RegisterViewController() <UITextFieldDelegate, LoginUIDelagate> {
    __weak IBOutlet UITextField *txtUsername;
    __weak IBOutlet UITextField *txtPassword;
    __weak IBOutlet UITextField *txtConfirmPassword;
    __weak IBOutlet UITextField *txtEmail;
    __weak IBOutlet UIButton *btnCreateAcc;
    __weak IBOutlet UIView *fieldView;
    
    LoginController *loginController;
    
    BOOL isKeyboardVisible;
}

@end

@implementation RegisterViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    loginController = [[LoginController alloc] init];
    loginController.uiLoginDelagate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

-(IBAction)createAccTapped:(id)sender{
    NSString *error;
    if([txtEmail.text isEqualToString:@""])
        error = @"Please enter Email Address";
    else if([txtPassword.text isEqualToString:@""])
        error = @"Please enter Password";
    else if([txtConfirmPassword.text isEqualToString:@""])
        error = @"Please enter Password again";
    else if([txtUsername.text isEqualToString:@""])
        error = @"Please enter Your Name";
    else if(![txtPassword.text isEqualToString:txtConfirmPassword.text])
        error = @"Passwords do not Match";
    else if(![self isValidEmail:txtEmail.text]){
        error = @"Please enter a valid Email Address";
    }
    
    if(error){
        [SharedMethods showMessage:@"Error" message:error onVC:self];
        return;
    }
    else{
        User *user = [[User alloc] init];
        user.email = txtEmail.text;
        user.password = txtPassword.text;
        user.name = txtUsername.text;
        [loginController registerAccount:user];
    }
}

-(BOOL) isValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == txtUsername){
        [txtEmail becomeFirstResponder];
        return YES;
    }
    else if(textField == txtEmail){
        [txtPassword becomeFirstResponder];
        return YES;
    } else if(textField == txtPassword){
        [txtConfirmPassword becomeFirstResponder];
        return YES;
    }
    else {
        [self.view endEditing:YES];
        [self createAccTapped:nil];
        return NO;
    }
}

-(void)registrationSuccess{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:nil];
    SetupViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SetupViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)registrationFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

#pragma mark - Keyboard Modification
- (void)keyboardDidShow:(NSNotification *)notif {
    if (isKeyboardVisible) {
        return;
    }
    CGRect curFrame = fieldView.frame;
    [UIView animateWithDuration:0.2 animations:^{
        fieldView.frame = CGRectMake(curFrame.origin.x, curFrame.origin.y - 120, curFrame.size.width, curFrame.size.height);
    }];
    
    isKeyboardVisible = YES;
}

- (void)keyboardDidHide:(NSNotification *)notif {
    if (!isKeyboardVisible) {
        return;
    }
    CGRect curFrame = fieldView.frame;
    [UIView animateWithDuration:0.2 animations:^{
        fieldView.frame = CGRectMake(curFrame.origin.x, curFrame.origin.y + 120, curFrame.size.width, curFrame.size.height);
    }];
    
    isKeyboardVisible = NO;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}


@end

#pragma mark - SetupViewController

@interface SetupViewController()<UIPickerViewDelegate, UIPickerViewDataSource, LoginUIDelagate, SettingsUIDelagate> {
    __weak IBOutlet UITextField *txtCarName;
    __weak IBOutlet UIPickerView *pckTrackingType;
    __weak IBOutlet UIButton *btnSubmit;
    
    int selectedTracking;
    LoginController *loginController;
    SettingsController *settingsController;
}

@end

@implementation SetupViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    loginController = [[LoginController alloc] init];
    loginController.uiLoginDelagate = self;
    settingsController = [[SettingsController alloc] init];
    settingsController.uiSettingsDelagate = self;
    
    selectedTracking = GPS_TRACKING;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    if(row == 0)
        title = @"GPS Tracking";
    else
        title = @"Manual Tracking";
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    return attString;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectedTracking = (int)row + 1;
}

-(IBAction)submitTapped:(id)sender{
    if([txtCarName.text isEqualToString:@""]){
        [SharedMethods showMessage:@"Error" message:@"Please enter Car Name" onVC:self];
        return;
    }
    
    Car *car = [[Car alloc] init];
    car.carName = txtCarName.text;
    
    [settingsController addCar:car];
}

-(void)addCarSuccess:(Car*)car{
    [SettingsDataStore getSharedInstance].car = car;
    User *user  = [[SettingsDataStore getSharedInstance].user copy];
    user.selectedCarId = [SettingsDataStore getSharedInstance].car.carId;
    user.trackingType = selectedTracking;
    
    [loginController updateAccount:user];
}

-(void)addCarFailure:(NSString*)msg{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)updateSuccess{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:[SettingsDataStore getSharedInstance].user.userId] forKey:@"userId"];
    [defaults synchronize];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MainVC *vc = [sb instantiateViewControllerWithIdentifier:@"MainVC"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)updateFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

@end
