//
//  SettingsController.h
//  IMileage
//
//  Created by Rajith Vithanage on 3/9/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "DBManager.h"

@protocol SettingsUIDelagate;

@interface SettingsController : NSObject {
    DBManager *dbManager;
}

@property(nonatomic,weak) id<SettingsUIDelagate> uiSettingsDelagate;

-(void)addCar:(Car*)car;
-(NSMutableArray*)getCarList;
-(Car*)getCarForId:(int)carId;
-(void)deleteCar:(int)carId;

@end

@protocol SettingsUIDelagate <NSObject>
-(void)addCarSuccess:(Car*)car;
-(void)addCarFailure:(NSString*)msg;
@optional
-(void)deleteCarSuccess;
@optional
-(void)deleteCarFailure:(NSString*)msg;
@end
