//
//  SettingsController.m
//  IMileage
//
//  Created by Rajith Vithanage on 3/9/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "SettingsController.h"
#import "SettingsDataStore.h"

@implementation SettingsController

@synthesize uiSettingsDelagate;

-(id) init{
    self = [super init];
    dbManager = [DBManager getSharedInstance];
    return self;
}

-(void)addCar:(Car*)car{
    NSArray *existing = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT car_name FROM car WHERE car_name = '%@' AND user_id = %d", car.carName, [SettingsDataStore getSharedInstance].user.userId]];
    if([existing count] == 0){
        NSString *query = [NSString stringWithFormat:@"INSERT INTO car(car_id, car_name, mileage, user_id) VALUES(NULL,'%@',0,%d)",car.carName,[SettingsDataStore getSharedInstance].user.userId];
        
        [dbManager executeQuery:query];
        
        if (dbManager.affectedRows != 0) {
            car.carId = (int)dbManager.lastInsertedRowID;
            [uiSettingsDelagate addCarSuccess:car];
        }
        else{
            [uiSettingsDelagate addCarFailure:@"Car Addition Failed"];
        }
    }
    else
        [uiSettingsDelagate addCarFailure:@"Car Name already exists"];
}

-(NSMutableArray*)getCarList{
    NSMutableArray *carArr = [[NSMutableArray alloc] init];
    NSArray *cars = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT car_id, car_name, mileage FROM car WHERE user_id = %d AND status = 1", [SettingsDataStore getSharedInstance].user.userId]];
    if([cars count] > 0){
        for(NSArray *data in cars){
            Car *car = [Car alloc];
            car.carId = [(NSString*)data[0] intValue];
            car.carName = (NSString*)data[1];
            car.mileage = [(NSString*)data[2] doubleValue];
            
            [carArr addObject:car];
        }
    }
    return carArr;
}

-(Car*)getCarForId:(int)carId{
    Car *car;
    NSArray *carArr = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT car_id, car_name, mileage FROM car WHERE car_id = %d", carId]];
    if([carArr count] > 0){
        NSArray *data = carArr[0];
        car = [Car alloc];
        car.carId = [(NSString*)data[0] intValue];
        car.carName = (NSString*)data[1];
        car.mileage = [(NSString*)data[2] doubleValue];
    }
    return car;
}

-(void)deleteCar:(int)carId{
    NSString *query = [NSString stringWithFormat:@"UPDATE car SET status = 0 WHERE car_id = %d",carId];
    
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
        [uiSettingsDelagate deleteCarSuccess];
    }
    else{
        [uiSettingsDelagate deleteCarFailure:@"Car Deletion Failed"];
    }
}

@end
