//
//  TripController.h
//  IMileage
//
//  Created by Rajith Vithanage on 3/9/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Trip.h"
#import "DBManager.h"

@protocol TripUIDelagate;

@interface TripController : NSObject {
    DBManager *dbManager;
}

@property(nonatomic,weak) id<TripUIDelagate> uiTripDelagate;

-(void)addTrip:(Trip*)trip;
-(Trip*)getTripForId:(int)tripId;
-(void)updateTrip:(Trip*)trip;
-(NSMutableArray*)getTripsFrom:(NSString*)fromDate to:(NSString*)toDate;
-(void)getTripDetails:(NSString*)fromDate to:(NSString*)toDate;
-(void)deleteTrip:(int)tripId;

@end

@protocol TripUIDelagate <NSObject>
@optional
-(void)addTripSuccess:(Trip*)trip;
@optional
-(void)addTripFailure:(NSString*)msg;
@optional
-(void)updateTripSuccess;
@optional
-(void)updateTripFailure:(NSString*)msg;
@optional
-(void)tripDetailsReceived:(double)distance count:(int)trips;
@optional
-(void)deleteTripSuccess;
@optional
-(void)deleteTripFailure:(NSString*)msg;
@end
