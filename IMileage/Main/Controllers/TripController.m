//
//  TripController.m
//  IMileage
//
//  Created by Rajith Vithanage on 3/9/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "TripController.h"
#import "SettingsDataStore.h"

@implementation TripController

@synthesize uiTripDelagate;

-(id) init{
    self = [super init];
    dbManager = [DBManager getSharedInstance];
    return self;
}

-(void)addTrip:(Trip*)trip{
    NSString *query = [NSString stringWithFormat:@"INSERT INTO trip(trip_id, tracking_mode, trip_type, start_loc, reason, car_id, user_id, trip_started_datetime) VALUES(NULL,%d,%d,'%@','%@',%d,%d, '%@')",trip.trakingType,trip.tripType,trip.startLocation,trip.reason,trip.carId,trip.userId,trip.startedDateTime];
    
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
        trip.tripId = (int)dbManager.lastInsertedRowID;
        [uiTripDelagate addTripSuccess:trip];
    }
    else{
        [uiTripDelagate addTripFailure:@"Trip Addition Failed"];
    }
}

-(void)updateTrip:(Trip*)trip{
    NSString *query = [NSString stringWithFormat:@"UPDATE trip SET distance = %.2f, end_loc = '%@', trip_time = %.2f, trip_end_datetime = '%@' WHERE trip_id = %d",trip.distance, trip.endLocation, trip.tripTime, trip.endDateTime, trip.tripId];
    
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
        [uiTripDelagate updateTripSuccess];
    }
    else{
        [uiTripDelagate updateTripFailure:@"Trip Update Failed"];
    }
}

-(Trip*)getTripForId:(int)tripId{
    NSArray *trip = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT trip_id, distance, tracking_mode, trip_type, start_loc, end_loc, trip_time, reason, car_id, user_id, trip_started_datetime, trip_end_datetime FROM trip WHERE trip_id = %d", tripId]];
    if([trip count] > 0){
        NSArray *data = trip[0];
        Trip *trip = [Trip alloc];
        trip.tripId = [(NSString*)data[0] intValue];
        trip.distance = [(NSString*)data[1] doubleValue];
        trip.trakingType = [(NSString*)data[2] intValue];
        trip.tripType = [(NSString*)data[3] intValue];
        trip.startLocation = (NSString*)data[4];
        trip.endLocation = (NSString*)data[5];
        trip.tripTime = [(NSString*)data[6] doubleValue];
        trip.reason = (NSString*)data[7];
        trip.carId = [(NSString*)data[8] intValue];
        trip.userId = [(NSString*)data[9] intValue];
        trip.startedDateTime = (NSString*)data[10];
        trip.endDateTime = (NSString*)data[11];
        
        return trip;
    }
    
    return nil;
}

-(NSMutableArray*)getTripsFrom:(NSString*)fromDate to:(NSString*)toDate{
    NSMutableArray *tripsArr = [[NSMutableArray alloc] init];
    NSArray *trips = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT trip_id, distance, tracking_mode, trip_type, start_loc, end_loc, trip_time, reason, car_id, user_id, trip_started_datetime, trip_end_datetime FROM trip WHERE trip_started_datetime >= '%@' AND trip_started_datetime <= '%@' AND user_id = %d AND status = 1 AND car_id = %d", fromDate, toDate, [SettingsDataStore getSharedInstance].user.userId, [SettingsDataStore getSharedInstance].user.selectedCarId]];
    if([trips count] > 0){
        for(NSArray *data in trips){
            Trip *trip = [Trip alloc];
            trip.tripId = [(NSString*)data[0] intValue];
            trip.distance = [(NSString*)data[1] doubleValue];
            trip.trakingType = [(NSString*)data[2] intValue];
            trip.tripType = [(NSString*)data[3] intValue];
            trip.startLocation = (NSString*)data[4];
            trip.endLocation = (NSString*)data[5];
            trip.tripTime = [(NSString*)data[6] doubleValue];
            trip.reason = (NSString*)data[7];
            trip.carId = [(NSString*)data[8] intValue];
            trip.userId = [(NSString*)data[9] intValue];
            trip.startedDateTime = (NSString*)data[10];
            trip.endDateTime = (NSString*)data[11];
            
            [tripsArr addObject:trip];
        }
    }
    return tripsArr;
}

-(void)getTripDetails:(NSString*)fromDate to:(NSString*)toDate{
    double distance = 0;
    int count = 0;
    NSArray *trips = [dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT COUNT(*), SUM(distance) FROM trip WHERE trip_started_datetime >= '%@' AND trip_started_datetime <= '%@' AND user_id = %d AND car_id = %d AND status = 1", fromDate, toDate, [SettingsDataStore getSharedInstance].user.userId,[SettingsDataStore getSharedInstance].car.carId]];
    if([trips count] > 0){
        count = [((NSArray*)trips[0])[0] intValue];
        distance = [((NSArray*)trips[0])[1] doubleValue];
    }
    
    [self.uiTripDelagate tripDetailsReceived:distance count:count];
}

-(void)deleteTrip:(int)tripId{
    NSString *query = [NSString stringWithFormat:@"UPDATE trip SET status = 0 WHERE trip_id = %d",tripId];
    
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
        [uiTripDelagate deleteTripSuccess];
    }
    else{
        [uiTripDelagate deleteTripFailure:@"Trip Deletion Failed"];
    }
}

@end
