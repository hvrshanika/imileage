//
//  GPSTrackingVC.h
//  IMileage
//
//  Created by Rajith Vithanage on 3/24/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Trip.h"

@interface GPSTrackingVC : UIViewController

@property (nonatomic, strong) Trip *currentTrip;

@end
