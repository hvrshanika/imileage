//
//  GPSTrackingVC.m
//  IMileage
//
//  Created by Rajith Vithanage on 3/24/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "GPSTrackingVC.h"
#import "TripController.h"
#import "SharedMethods.h"
#import "SettingsDataStore.h"
#import <MapKit/MapKit.h>

@interface GPSTrackingVC()<UITextFieldDelegate, TripUIDelagate, CLLocationManagerDelegate, MKMapViewDelegate> {
    __weak IBOutlet UIButton *btnStartEnd;
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UIView *viewTripType;
    __weak IBOutlet UIView *viewStarted;
    __weak IBOutlet UIView *viewTravelled;
    __weak IBOutlet UILabel *lblTravelled;
    __weak IBOutlet UILabel *lblStartedAt;
    __weak IBOutlet UILabel *lblTripType;
    __weak IBOutlet UIImageView *imgTripType;
    __weak IBOutlet MKMapView *mapView;
    
    BOOL tripStarted;
    UIAlertAction* business;
    UIAlertAction* personal;
    
    TripController *tripController;
    
    CLLocationManager *locationManager;
    CLGeocoder *ceo;
    CLLocationCoordinate2D coordinate;
    
    MKPolyline* routeLine;
    MKPolylineRenderer* routeLineView;
    CLLocation *previousLocation;
    
    double totalDistance;
}

@end

@implementation GPSTrackingVC

-(void)viewDidLoad{
    [super viewDidLoad];
    
    tripController = [[TripController alloc] init];
    tripController.uiTripDelagate = self;
    
    [self setLabels];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(!locationManager){
        locationManager = [[CLLocationManager alloc]init];
        locationManager.delegate = self;
        ceo= [[CLGeocoder alloc]init];
    }
    
    mapView.showsUserLocation = YES;
    [self getCurrentLocation];
}

-(void)getCurrentLocation{
    [locationManager requestWhenInUseAuthorization];
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    coordinate.latitude=locationManager.location.coordinate.latitude;
    coordinate.longitude=locationManager.location.coordinate.longitude;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 500, 500);
    [mapView setRegion:viewRegion animated:YES];
    
    [locationManager stopUpdatingLocation];
}

-(void)getAddressForCordinate:(CLLocationCoordinate2D)coordinate label:(UILabel*)lbl{
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  lbl.text = [NSString stringWithFormat:@"%@ %@", placemark.name ? placemark.name : placemark.thoroughfare, placemark.locality ? placemark.locality : @""];
              }
     ];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    if(tripStarted){
        CLLocation *newLocation = [locations objectAtIndex:locations.count - 1];
        
        double distance = [newLocation distanceFromLocation:previousLocation]/1000;
        totalDistance += distance;
        lblTravelled.text = [NSString stringWithFormat:@"%.2f KMs",totalDistance < 0 ? -totalDistance : totalDistance];
        
        if(distance > 0){
            CLLocationCoordinate2D coordinateArray[2];
            coordinateArray[0] = previousLocation.coordinate;
            coordinateArray[1] = coordinate = newLocation.coordinate;
            
            routeLine = [MKPolyline polylineWithCoordinates:coordinateArray count:2];
            
            if (newLocation.coordinate.latitude - previousLocation.coordinate.latitude < 1)
                [mapView addOverlay:routeLine];
        }
        
        previousLocation = newLocation;
    }
}

-(MKOverlayRenderer*)mapView:(MKMapView*)mapView rendererForOverlay:(id <MKOverlay>)overlay{
    if(overlay == routeLine){
        routeLineView = [[MKPolylineRenderer alloc] initWithPolyline:routeLine];
        routeLineView.strokeColor = [UIColor blueColor];
        routeLineView.lineWidth = 7;
        return routeLineView;
    }
    return nil;
}

-(void)setLabels{
    if(self.currentTrip.tripType == PERSONAL_TRIP){
        lblTripType.text = @"Personal Trip";
        imgTripType.image = [UIImage imageNamed:@"ic_personal.png"];
    }
    else{
        lblTripType.text = @"Business Trip";
        imgTripType.image = [UIImage imageNamed:@"ic_business.png"];
    }
    
    if(self.currentTrip.tripId > 0){
        tripStarted = YES;
        viewStarted.hidden = NO;
        lblStartedAt.text = self.currentTrip.startLocation;
        [btnStartEnd setTitle:@"END TRIP" forState:UIControlStateNormal];
    }
}

-(IBAction)startBtnTapped:(id)sender{
    NSDateFormatter *formatter = [[SettingsDataStore getSharedInstance] getTripDateTimeFormatter];
    if(tripStarted){
        tripStarted = NO;
        [mapView setUserTrackingMode:MKUserTrackingModeNone animated:YES];
        [locationManager stopUpdatingLocation];
        
        self.currentTrip.endLocation = [NSString stringWithFormat:@"%f,%f",coordinate.latitude,coordinate.longitude];
        self.currentTrip.distance = totalDistance < 0 ? -totalDistance : totalDistance;
        
//        if(self.currentTrip.distance == 0){
//            NSArray *arr = [self.currentTrip.startLocation componentsSeparatedByString:@","];
//            CLLocationCoordinate2D start;
//            start.latitude = [arr[0] doubleValue];
//            start.longitude = [arr[1] doubleValue];
//            
//            MKMapPoint point1 = MKMapPointForCoordinate(coordinate);
//            MKMapPoint point2 = MKMapPointForCoordinate(start);
//            CLLocationDistance distance = MKMetersBetweenMapPoints(point1, point2);
//            self.currentTrip.distance = (distance < 0 ? -distance : distance)/1000;
//        }
        
        self.currentTrip.endDateTime = [formatter stringFromDate:[NSDate date]];
        
        NSDate *started = [formatter dateFromString:self.currentTrip.startedDateTime];
        int timeMillis = [[NSDate date] timeIntervalSinceDate:started];
        self.currentTrip.tripTime = (double)timeMillis/60;
        
        [tripController updateTrip:self.currentTrip];
    }
    else{
        tripStarted = YES;
        viewStarted.alpha = 0;
        viewStarted.hidden = NO;
        viewTravelled.alpha = 0;
        viewTravelled.hidden = NO;
        
        [self getCurrentLocation];
        [self getAddressForCordinate:coordinate label:lblStartedAt];
        
        [btnStartEnd setTitle:@"END TRIP" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.5 animations:^{
            viewStarted.alpha = 1;
            viewTravelled.alpha = 1;
        } completion:nil];
        
        [locationManager startUpdatingLocation];
        [mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
        
        self.currentTrip.startedDateTime = [formatter stringFromDate:[NSDate date]];
        self.currentTrip.startLocation = [NSString stringWithFormat:@"%f,%f",coordinate.latitude,coordinate.longitude];
        [tripController addTrip:self.currentTrip];
    }
}

-(IBAction)closeBtnTapped:(id)sender{
    if(tripStarted){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"End Trip" message:@"Are you sure you want to End this Trip?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *yes = [UIAlertAction
                              actionWithTitle:@"Yes"
                              style:UIAlertActionStyleDestructive
                              handler:^(UIAlertAction * action)
                              {
                                  [self startBtnTapped:nil];
                              }];
        [alert addAction:yes];
        
        UIAlertAction *no = [UIAlertAction
                             actionWithTitle:@"No"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:no];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(IBAction)changeTripType:(id)sender{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = self.currentTrip.reason;
        textField.delegate = self;
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    }];
    
    business = [UIAlertAction
                actionWithTitle:@"Business Trip"
                style:UIAlertActionStyleDefault
                handler:^(UIAlertAction * action)
                {
                    if(![[alert textFields][0].text isEqualToString:@""]){
                        self.currentTrip.tripType = BUSINESS_TRIP;
                        self.currentTrip.reason = [alert textFields][0].text;
                        [self setLabels];
                    }
                    
                }];
    
    [alert addAction:business];
    
    personal = [UIAlertAction
                actionWithTitle:@"Personal Trip"
                style:UIAlertActionStyleDefault
                handler:^(UIAlertAction * action)
                {
                    if(![[alert textFields][0].text isEqualToString:@""]){
                        self.currentTrip.tripType = PERSONAL_TRIP;
                        self.currentTrip.reason = [alert textFields][0].text;
                        [self setLabels];
                    }
                }];
    
    [alert addAction:personal];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(finalString.length > 0){
        [personal setEnabled:YES];
        [business setEnabled:YES];
    }
    else{
        [personal setEnabled:NO];
        [business setEnabled:NO];
    }
    return YES;
}

-(void)addTripSuccess:(Trip*)trip{
    self.currentTrip = trip;
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:trip.tripId] forKey:@"currentTripID"];
    [defaults synchronize];
}

-(void)addTripFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

-(void)updateTripSuccess{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:0] forKey:@"currentTripID"];
    [defaults synchronize];
    [self closeBtnTapped:nil];
}

-(void)updateTripFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

@end
