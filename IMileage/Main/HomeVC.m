//
//  HomeVC.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "HomeVC.h"
#import "Trip.h"
#import "ManualTracking.h"
#import "SettingsDataStore.h"
#import "TripController.h"
#import "GPSTrackingVC.h"

@interface HomeVC() <UITextFieldDelegate, TripUIDelagate> {
    __weak IBOutlet UILabel *lblTripsCount;
    __weak IBOutlet UILabel *lblMonth;
    __weak IBOutlet UILabel *lblMileageCount;
    __weak IBOutlet UIButton *btnStartTracking;
    
    int selectedTripType;
    NSString *reason;
    
    UIAlertAction* business;
    UIAlertAction* personal;
    
    Trip *currentTrip;
    TripController *tripController;
    
    NSDateFormatter *dateFormatter;
}

@end

@implementation HomeVC

-(void)viewDidLoad{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(!tripController){
        tripController = [[TripController alloc] init];
        tripController.uiTripDelagate = self;
    }
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    int tripId = [(NSNumber*)[defaults objectForKey:@"currentTripID"] intValue];
    
    if(tripId > 0){
        [btnStartTracking setTitle:@"STOP TRACKING" forState:UIControlStateNormal];
        currentTrip = [tripController getTripForId:tripId];
    }
    else{
        [btnStartTracking setTitle:@"START TRACKING" forState:UIControlStateNormal];
        currentTrip = nil;
    }
    
    if(!dateFormatter){
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    }
    
    NSString *from = [self getFromDateString:[self dateAtStartOfMonth]];
    NSString *to = [self getToDateString:[NSDate date]];
    [tripController getTripDetails:from to:to];
}

-(void)tripDetailsReceived:(double)distance count:(int)trips{
    lblMileageCount.text = [NSString stringWithFormat:@"%.f",distance];
    lblTripsCount.text = [NSString stringWithFormat:@"%d",trips];
    
    [dateFormatter setDateFormat:@"LLLL yyyy"];
    lblMonth.text = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
}

-(IBAction)startBtnTapped:(id)sender{
    if(currentTrip){
        [self openTrackingUI];
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Reason";
        textField.delegate = self;
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    }];
    
    business = [UIAlertAction
                                actionWithTitle:@"Business Trip"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                               {
                                   if(![[alert textFields][0].text isEqualToString:@""]){
                                       selectedTripType = BUSINESS_TRIP;
                                       reason = [alert textFields][0].text;
                                       [self setTripDetails];
                                   }
                                   
                               }];

    [business setEnabled:NO];
    [alert addAction:business];
    
    personal = [UIAlertAction
                                actionWithTitle:@"Personal Trip"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                               {
                                   if(![[alert textFields][0].text isEqualToString:@""]){
                                       reason = [alert textFields][0].text;
                                       selectedTripType = PERSONAL_TRIP;
                                       [self setTripDetails];
                                   }
                               }];
    [personal setEnabled:NO];
    [alert addAction:personal];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(finalString.length > 0){
        [personal setEnabled:YES];
        [business setEnabled:YES];
    }
    else{
        [personal setEnabled:NO];
        [business setEnabled:NO];
    }
    return YES;
}

-(void)setTripDetails{
    Trip *trip = [[Trip alloc] init];
    trip.tripType = selectedTripType;
    trip.reason = reason;
    trip.trakingType = [SettingsDataStore getSharedInstance].user.trackingType;
    trip.userId = [SettingsDataStore getSharedInstance].user.userId;
    trip.carId = [SettingsDataStore getSharedInstance].user.selectedCarId;
    
    currentTrip = trip;
    [self openTrackingUI];
}

-(void)openTrackingUI{
    if([SettingsDataStore getSharedInstance].user.trackingType == GPS_TRACKING){
        [self openGPSTrackingVC:currentTrip];
    }
    else{
        [self openManualTrackingVC:currentTrip];
    }
}

-(void)openManualTrackingVC:(Trip*)trip{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    ManualTracking *vc = [storyboard instantiateViewControllerWithIdentifier:@"ManualTracking"];
    vc.currentTrip = trip;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)openGPSTrackingVC:(Trip*)trip{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    GPSTrackingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"GPSTrackingVC"];
    vc.currentTrip = trip;
    [self presentViewController:vc animated:YES completion:nil];
}

-(NSString*)getFromDateString:(NSDate*)date{
    NSMutableString *dateStr = [[NSMutableString alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateStr appendString:[dateFormatter stringFromDate:date]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    [dateStr appendString:@" 00:00:00"];
    return dateStr;
}

-(NSString*)getToDateString:(NSDate*)date{
    NSMutableString *dateStr = [[NSMutableString alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateStr appendString:[dateFormatter stringFromDate:date]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    [dateStr appendString:@" 23:59:59"];
    return dateStr;
}

- (NSDate *) dateAtStartOfMonth{
    NSDateComponents *comp = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    [comp setDay:1];
    return [[NSCalendar currentCalendar] dateFromComponents:comp];
}


@end
