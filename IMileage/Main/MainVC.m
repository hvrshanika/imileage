//
//  MainVC.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "MainVC.h"
#import "LoginController.h"
#import "SettingsController.h"
#import "SettingsDataStore.h"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad{
    [super viewDidLoad];
    
    if(![SettingsDataStore getSharedInstance].user){
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        int userId = [(NSNumber*)[defaults objectForKey:@"userId"] intValue];
        
        LoginController *loginController = [[LoginController alloc] init];
        [SettingsDataStore getSharedInstance].user = [loginController getUserForId:userId];
        
        SettingsController *sController = [[SettingsController alloc] init];
        [SettingsDataStore getSharedInstance].car = [sController getCarForId:[SettingsDataStore getSharedInstance].user.selectedCarId];
    }
}

@end
