//
//  ManualTracking.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "ManualTracking.h"
#import "TripController.h"
#import "SharedMethods.h"
#import "SettingsDataStore.h"

@interface ManualTracking()<UITextFieldDelegate, TripUIDelagate> {
    __weak IBOutlet UITextField *txtMeterReading;
    __weak IBOutlet UIButton *btnStartEnd;
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UIView *viewTripType;
    __weak IBOutlet UIView *viewStarted;
    __weak IBOutlet UILabel *lblStartedAt;
    __weak IBOutlet UILabel *lblTripType;
    __weak IBOutlet UIImageView *imgTripType;
    
    BOOL tripStarted;
    UIAlertAction* business;
    UIAlertAction* personal;
    
    TripController *tripController;
}

@end

@implementation ManualTracking

-(void)viewDidLoad{
    [super viewDidLoad];
    
    tripController = [[TripController alloc] init];
    tripController.uiTripDelagate = self;
    
    [self setLabels];
}

-(void)setLabels{
    if(self.currentTrip.tripType == PERSONAL_TRIP){
        lblTripType.text = @"Personal Trip";
        imgTripType.image = [UIImage imageNamed:@"ic_personal.png"];
    }
    else{
        lblTripType.text = @"Business Trip";
        imgTripType.image = [UIImage imageNamed:@"ic_business.png"];
    }
    
    if(self.currentTrip.tripId > 0){
        tripStarted = YES;
        viewStarted.hidden = NO;
        lblStartedAt.text = self.currentTrip.startLocation;
        [btnStartEnd setTitle:@"END TRIP" forState:UIControlStateNormal];
    }
}

-(IBAction)closeBtnTapped:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)startBtnTapped:(id)sender{
    [self.view endEditing:YES];
    if(txtMeterReading.text != nil && ![txtMeterReading.text isEqualToString:@""]){
        NSDateFormatter *formatter = [[SettingsDataStore getSharedInstance] getTripDateTimeFormatter];
        if(tripStarted){
            if([txtMeterReading.text doubleValue] < [self.currentTrip.startLocation doubleValue]){
                [SharedMethods showMessage:@"Error" message:@"End Meter Reading should be greater than the Start Meter Reading" onVC:self];
                return;
            }
            
            self.currentTrip.endLocation = txtMeterReading.text;
            self.currentTrip.distance = [self.currentTrip.endLocation doubleValue] - [self.currentTrip.startLocation doubleValue];
            
            self.currentTrip.endDateTime = [formatter stringFromDate:[NSDate date]];
            
            NSDate *started = [formatter dateFromString:self.currentTrip.startedDateTime];
            int timeMillis = [[NSDate date] timeIntervalSinceDate:started];
            self.currentTrip.tripTime = (double)timeMillis/60;
            
            [tripController updateTrip:self.currentTrip];
        }
        else{
            tripStarted = YES;
            viewStarted.alpha = 0;
            viewStarted.hidden = NO;
            lblStartedAt.text = txtMeterReading.text;
            txtMeterReading.text = @"";
            [btnStartEnd setTitle:@"END TRIP" forState:UIControlStateNormal];
            [UIView animateWithDuration:0.5 animations:^{
                viewStarted.alpha = 1;
            } completion:nil];
            
            self.currentTrip.startedDateTime = [formatter stringFromDate:[NSDate date]];
            self.currentTrip.startLocation = lblStartedAt.text;
            [tripController addTrip:self.currentTrip];
        }
    }
    else{
        [SharedMethods showMessage:@"Error" message:@"Please Enter the Odometer Reading" onVC:self];
    }
}

-(IBAction)changeTripType:(id)sender{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = self.currentTrip.reason;
        textField.delegate = self;
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    }];
    
    business = [UIAlertAction
                actionWithTitle:@"Business Trip"
                style:UIAlertActionStyleDefault
                handler:^(UIAlertAction * action)
                {
                    if(![[alert textFields][0].text isEqualToString:@""]){
                        self.currentTrip.tripType = BUSINESS_TRIP;
                        self.currentTrip.reason = [alert textFields][0].text;
                        [self setLabels];
                    }
                    
                }];
    
    [alert addAction:business];
    
    personal = [UIAlertAction
                actionWithTitle:@"Personal Trip"
                style:UIAlertActionStyleDefault
                handler:^(UIAlertAction * action)
                {
                    if(![[alert textFields][0].text isEqualToString:@""]){
                        self.currentTrip.tripType = PERSONAL_TRIP;
                        self.currentTrip.reason = [alert textFields][0].text;
                        [self setLabels];
                    }
                }];

    [alert addAction:personal];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(finalString.length > 0){
        [personal setEnabled:YES];
        [business setEnabled:YES];
    }
    else{
        [personal setEnabled:NO];
        [business setEnabled:NO];
    }
    return YES;
}

-(void)addTripSuccess:(Trip*)trip{
    self.currentTrip = trip;
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:trip.tripId] forKey:@"currentTripID"];
    [defaults synchronize];
}

-(void)addTripFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

-(void)updateTripSuccess{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:0] forKey:@"currentTripID"];
    [defaults synchronize];
    [self closeBtnTapped:nil];
}

-(void)updateTripFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

@end
