//
//  ReportVC.h
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ReportVC : UIViewController

@end

@interface ReportCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *lblDuration;
@property (nonatomic, weak) IBOutlet UILabel *lblreason;
@property (nonatomic, weak) IBOutlet UILabel *lblDistance;

@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;

@end
