//
//  ReportVC.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "ReportVC.h"
#import "TripController.h"
#import "Trip.h"
#import "SharedMethods.h"
#import "PDFWriter.h"
#import "WriteToCSVFile.h"
#import "SettingsDataStore.h"
#import <MessageUI/MFMailComposeViewController.h>

#pragma mark - Report Cell

@implementation ReportCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

#pragma mark - ReportVC

@interface ReportVC()<UITableViewDataSource, TripUIDelagate, UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate> {
    __weak IBOutlet UIButton *btnEMail;
    __weak IBOutlet UIButton *btnFromDate;
    __weak IBOutlet UIButton *btnToDate;
    __weak IBOutlet UILabel *lblFromDate;
    __weak IBOutlet UILabel *lblToDate;
    __weak IBOutlet UITableView *table;
    __weak IBOutlet UISegmentedControl *segmentControl;
    
    NSDate *toDate;
    NSDate *fromDate;
    
    NSMutableArray *trips;
    NSMutableArray *filteredTrips;
    TripController *tripController;
    
    int selectedSegment;
    
    NSDateFormatter *dateFormatter;
    
    UIDatePicker *datePicker;
    UIView *topOverlay;
    UIView *datePickerContainer;
}

@end

@implementation ReportVC

-(void)viewDidLoad{
    [super viewDidLoad];
    
    selectedSegment = 2;
    
    tripController = [[TripController alloc] init];
    tripController.uiTripDelagate = self;
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    comps.day = -7;
    fromDate = [calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    toDate = [NSDate date];
    lblFromDate.text = [dateFormatter stringFromDate:fromDate];
    lblToDate.text = [dateFormatter stringFromDate:toDate];
    
    table.tableFooterView = [[UIView alloc] init];
    
    [self setUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(tripController)
        [self setUI];
}

-(void)setUI{
    NSString *from = [self getFromDateString:fromDate];
    NSString *to = [self getToDateString:toDate];
    trips = [tripController getTripsFrom:from to:to];
    [self segmentChanged:nil];
}

-(IBAction)emailBtnTapped:(id)sender{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *pdf = [UIAlertAction
                          actionWithTitle:@"PDF"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              [self createFileAndEmail:1];
                          }];
    [alert addAction:pdf];
    
    UIAlertAction *csv = [UIAlertAction
                         actionWithTitle:@"CSV"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self createFileAndEmail:2];
                         }];
    [alert addAction:csv];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)segmentChanged:(id)sender {
    selectedSegment = (int)segmentControl.selectedSegmentIndex;
    if(selectedSegment == 2)
        filteredTrips = trips;
    else{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tripType == %d",selectedSegment+1];
        filteredTrips = [[trips filteredArrayUsingPredicate:predicate] mutableCopy];
    }
    [table reloadData];
}

-(IBAction)showDatePicker:(UIButton*)sender{
    datePickerContainer = [[UIView alloc] init];
    datePickerContainer.backgroundColor = [UIColor whiteColor];
    datePickerContainer.frame = CGRectMake(10, 130, 355, 216);
    datePickerContainer.layer.cornerRadius = 5;
    datePickerContainer.clipsToBounds = YES;
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 355, 30)];
    title.text = sender.tag == 1 ? @"Select From Date" : @"Select To Date";
    title.font = [UIFont systemFontOfSize:20];
    [datePickerContainer addSubview:title];
    
    UIButton *btnDone = [[UIButton alloc] initWithFrame:CGRectMake(275, 5, 80, 30)];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    [btnDone addTarget:self action:@selector(dateSelected:) forControlEvents:UIControlEventTouchUpInside];
    [datePickerContainer addSubview:btnDone];
    
    CGRect pickerFrame = CGRectMake(10, 30, 355, 216);
    UIDatePicker *pView = [[UIDatePicker alloc] initWithFrame:pickerFrame];
    pView.date = sender.tag == 1 ? fromDate : toDate;
    pView.datePickerMode = UIDatePickerModeDate;
    pView.maximumDate = [NSDate date];
    pView.tag = sender.tag;
    
    datePicker = pView;
    [datePickerContainer addSubview:datePicker];
    
    topOverlay = [[UIView alloc] init];
    topOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    topOverlay.frame = self.view.frame;
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [topOverlay addGestureRecognizer:singleFingerTap];
    
    [self.view addSubview:topOverlay];
    [self.view addSubview:datePickerContainer];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer{
    [datePickerContainer removeFromSuperview];
    [topOverlay removeFromSuperview];
}

- (IBAction) dateSelected:(id)sender{
    [datePickerContainer removeFromSuperview];
    [topOverlay removeFromSuperview];
    
    if((int)datePicker.tag == 1){
        if(!([datePicker.date compare:toDate] == NSOrderedAscending || [datePicker.date compare:toDate] == NSOrderedSame)){
            [SharedMethods showMessage:@"Warning" message:@"From Date should be a earlier date" onVC:self];
            return;
        }
        fromDate = datePicker.date;
        lblFromDate.text = [dateFormatter stringFromDate:fromDate];
    }
    else{
        if(!([datePicker.date compare:fromDate] == NSOrderedDescending || [datePicker.date compare:fromDate] == NSOrderedSame)){
            [SharedMethods showMessage:@"Warning" message:@"To Date should be a later date" onVC:self];
            return;
        }
        toDate = datePicker.date;
        lblToDate.text = [dateFormatter stringFromDate:toDate];
    }
    [self setUI];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([filteredTrips count] > 0){
        tableView.backgroundView = nil;
        return 1;
    }
    else{
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.bounds.size.width - 5, self.view.bounds.size.height)];
        
        messageLabel.text = [NSString stringWithFormat:@"%@",
                             NSLocalizedString(@"No Reports to show.", nil)];
        messageLabel.textColor = [UIColor darkGrayColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:15];
        [messageLabel sizeToFit];
        
        tableView.backgroundView = messageLabel;
        return 0;
    }
        
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [filteredTrips count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportCell *cell = (ReportCell *)[tableView dequeueReusableCellWithIdentifier:@"reportCell"];
    Trip *trip = [filteredTrips objectAtIndex:indexPath.row];
    
    NSArray *arr = [trip.startedDateTime componentsSeparatedByString:@" "];
    
    NSString *dateStr;
    if([arr count] > 0){
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString:arr[0]];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        dateStr = [dateFormatter stringFromDate:date];
    }
    else{
        dateStr = @"";
    }
    
    cell.lblDate.text = dateStr;
    cell.lblreason.text = trip.reason;
    cell.lblDistance.text = [NSString stringWithFormat:@"%.2f km",trip.distance];
    cell.lblDuration.text = [NSString stringWithFormat:@"%.2f hours",trip.tripTime/60];
    
    return cell;
}

- (void)tableView:(UITableView*)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Trip" message:@"Are you sure you want to delete this Trip?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yes = [UIAlertAction
                actionWithTitle:@"Yes"
                style:UIAlertActionStyleDestructive
                handler:^(UIAlertAction * action)
                {
                    Trip *trip = [filteredTrips objectAtIndex:indexPath.row];
                    [tripController deleteTrip:trip.tripId];
                }];
    [alert addAction:yes];
    
    UIAlertAction *no = [UIAlertAction
                actionWithTitle:@"No"
                style:UIAlertActionStyleCancel
                handler:^(UIAlertAction * action)
                {
                    [alert dismissViewControllerAnimated:YES completion:nil];
                }];
    [alert addAction:no];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)deleteTripSuccess{
    [self setUI];
}

-(void)deleteTripFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

-(NSString*)getFromDateString:(NSDate*)date{
    NSMutableString *dateStr = [[NSMutableString alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateStr appendString:[dateFormatter stringFromDate:date]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    [dateStr appendString:@" 00:00:00"];
    return dateStr;
}

-(NSString*)getToDateString:(NSDate*)date{
    NSMutableString *dateStr = [[NSMutableString alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateStr appendString:[dateFormatter stringFromDate:date]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    [dateStr appendString:@" 23:59:59"];
    return dateStr;
}

#pragma mark - Email

-(void)createFileAndEmail:(int)mode{
    NSString *fulpath=@"";
    
    @try {
        if(mode == 1){
            NSString *fileName = [NSString stringWithFormat:@"TripReport%@TO%@.pdf",[dateFormatter stringFromDate:fromDate],[dateFormatter stringFromDate:toDate]];
            NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDir = [documentPaths objectAtIndex:0];
            fulpath = [documentsDir stringByAppendingPathComponent:fileName];
            
            PDFWriter *writer = [[PDFWriter alloc] initWithFileName:fulpath withOrientation:FileOrientationPortrait];
            [writer drawTitle:@"iMileage" centerAlign:YES percentageOffser:0];
            [writer drawHeader:NSLocalizedString(@"Trip Report", nil) centerAlign:YES percentageOffser:0];
            [writer drawHeader:[NSString stringWithFormat:@"%@ - %@",[SettingsDataStore getSharedInstance].user.name, [SettingsDataStore getSharedInstance].car.carName] centerAlign:YES percentageOffser:0];
            
            NSString *tripType = @"";
            if(selectedSegment == 2)
                tripType = @"All Trips";
            else if(selectedSegment+1 == BUSINESS_TRIP)
                tripType = @"Business Trips";
            else if(selectedSegment+1 == PERSONAL_TRIP)
                tripType = @"Personal Trips";
            [writer drawHeader:tripType centerAlign:YES percentageOffser:0];
            [writer drawHeader:[NSString stringWithFormat:@"%@ %@ %@ %@", @"From" ,[dateFormatter stringFromDate:fromDate], @"To", [dateFormatter stringFromDate:toDate]] centerAlign:YES percentageOffser:0];
            
            [writer drawEmptyLine];
            
            NSMutableArray *colHeaders = [[NSMutableArray alloc] initWithCapacity:3];
            [colHeaders addObject:NSLocalizedString(@"Trip Date", nil)];
            [colHeaders addObject:NSLocalizedString(@"Reason", nil)];
            [colHeaders addObject:NSLocalizedString(@"Start Time", nil)];
            [colHeaders addObject:NSLocalizedString(@"End Time", nil)];
            [colHeaders addObject:NSLocalizedString(@"Duration", nil)];
            [colHeaders addObject:NSLocalizedString(@"Distance", nil)];
            
            NSMutableArray *colWidths = [[NSMutableArray alloc] initWithCapacity:3];
            [colWidths addObject:[NSNumber numberWithInt:15]];
            [colWidths addObject:[NSNumber numberWithInt:25]];
            [colWidths addObject:[NSNumber numberWithInt:15]];
            [colWidths addObject:[NSNumber numberWithInt:15]];
            [colWidths addObject:[NSNumber numberWithInt:15]];
            [colWidths addObject:[NSNumber numberWithInt:15]];
            
            NSMutableArray *colAlign = [[NSMutableArray alloc] initWithCapacity:3];
            [colAlign addObject:[NSNumber numberWithInt:0]];
            [colAlign addObject:[NSNumber numberWithInt:0]];
            [colAlign addObject:[NSNumber numberWithInt:0]];
            [colAlign addObject:[NSNumber numberWithInt:0]];
            [colAlign addObject:[NSNumber numberWithInt:0]];
            [colAlign addObject:[NSNumber numberWithInt:2]];
            
            NSMutableArray *fullArray =  [[NSMutableArray alloc] initWithCapacity:10];
            for(int i = 0; i < [filteredTrips count]; i++){
                Trip *trip = [filteredTrips objectAtIndex:i];
                
                NSMutableArray *array2 = [NSMutableArray array];
                NSArray *arr = [trip.startedDateTime componentsSeparatedByString:@" "];
                NSArray *arr1 = [trip.endDateTime componentsSeparatedByString:@" "];
                [array2 addObject: [arr count] > 0 ? arr[0] : @""];
                [array2 addObject: trip.reason];
                [array2 addObject: [arr count] > 1 ? arr[1] : @""];
                [array2 addObject: [arr1 count] > 1 ? arr1[1] : @""];
                [array2 addObject: [NSString stringWithFormat:@"%.2f hours",trip.tripTime/60]];
                [array2 addObject: [NSString stringWithFormat:@"%.2f kM",trip.distance]];
                
                [fullArray addObject:array2];
            }
            
            [writer drawDataTable:fullArray withColumnHeaders:colHeaders withColumnPercentageWidths:colWidths withColumnAlignments:colAlign];
            
            [writer finishPDFFile];
            
            
        } else if(mode == 2){
            NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDir = [documentPaths objectAtIndex:0];

            NSString *fileane = [NSString stringWithFormat:@"TripReport%@TO%@.csv",[dateFormatter stringFromDate:fromDate],[dateFormatter stringFromDate:toDate]];
            fulpath = [documentsDir stringByAppendingPathComponent:fileane];

            NSMutableArray *fullArray =  [[NSMutableArray alloc] initWithCapacity:10];

            NSMutableArray *empty = [[NSMutableArray alloc] initWithCapacity:3];
            [empty addObject:@""];

            NSMutableArray *array00 = [[NSMutableArray alloc] initWithCapacity:3];
            [array00 addObject:@"iMileage"];
            [fullArray addObject:array00];

            [fullArray addObject:empty];

            NSMutableArray *array01 = [[NSMutableArray alloc] initWithCapacity:3];
            [array01 addObject:NSLocalizedString(@"Trip Report", nil)];
            [fullArray addObject:array01];

            NSMutableArray *array011 = [NSMutableArray array];
            [array011 addObject:[NSString stringWithFormat:@"%@ - %@",[SettingsDataStore getSharedInstance].user.name, [SettingsDataStore getSharedInstance].car.carName]];
            [fullArray addObject:array011];
            
            NSString *tripType = @"";
            if(selectedSegment == 2)
                tripType = @"All Trips";
            else if(selectedSegment+1 == BUSINESS_TRIP)
                tripType = @"Business Trips";
            else if(selectedSegment+1 == PERSONAL_TRIP)
                tripType = @"Personal Trips";
            NSMutableArray *array012 = [[NSMutableArray alloc] initWithCapacity:3];
            [array012 addObject:tripType];
            [fullArray addObject:array012];

            NSMutableArray *array10 = [[NSMutableArray alloc] initWithCapacity:3];
            [array10 addObject:[NSString stringWithFormat:@"%@ %@ %@ %@", @"From" ,[dateFormatter stringFromDate:fromDate], @"To", [dateFormatter stringFromDate:toDate]]];
            [fullArray addObject:array10];

            [fullArray addObject:empty];

            NSMutableArray *array1 = [[NSMutableArray alloc] initWithCapacity:3];
            [array1 addObject:NSLocalizedString(@"Trip Date", nil)];
            [array1 addObject:NSLocalizedString(@"Reason", nil)];
            [array1 addObject:NSLocalizedString(@"Start Time", nil)];
            [array1 addObject:NSLocalizedString(@"End Time", nil)];
            [array1 addObject:NSLocalizedString(@"Duration", nil)];
            [array1 addObject:NSLocalizedString(@"Distance", nil)];

            [fullArray addObject:array1];

            for(int i = 0; i < [filteredTrips count]; i++){
                Trip *trip = [filteredTrips objectAtIndex:i];
                
                NSMutableArray *array2 = [NSMutableArray array];
                NSArray *arr = [trip.startedDateTime componentsSeparatedByString:@" "];
                NSArray *arr1 = [trip.endDateTime componentsSeparatedByString:@" "];
                [array2 addObject: [arr count] > 0 ? arr[0] : @""];
                [array2 addObject: trip.reason];
                [array2 addObject: [arr count] > 1 ? arr[1] : @""];
                [array2 addObject: [arr1 count] > 1 ? arr1[1] : @""];
                [array2 addObject: [NSString stringWithFormat:@"%.2f hours",trip.tripTime/60]];
                [array2 addObject: [NSString stringWithFormat:@"%.2f kM",trip.distance]];
                
                [fullArray addObject:array2];
            }

            [WriteToCSVFile writeToCSVFile:fullArray FilePath:fulpath];
        }
    }
    @catch (NSException * e) {
        [SharedMethods showMessage:@"Error" message:@"Please try again" onVC:self];
        return;
    }
    
    @try {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [mailViewController setSubject:NSLocalizedString(@"Trip Report", nil)];
            [mailViewController setMessageBody:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Trip Report", nil), @"Generated By iMileage"] isHTML:NO];
            
            NSData *myData = [NSData dataWithContentsOfFile:fulpath];
            
            if(mode == 1){
                NSString *fileane = [NSString stringWithFormat:@"TripReport%@TO%@.pdf",[dateFormatter stringFromDate:fromDate],[dateFormatter stringFromDate:toDate]];
                [mailViewController addAttachmentData:myData mimeType:@"application/pdf" fileName:fileane];
            }else if (mode == 2){
                NSString *fileane = [NSString stringWithFormat:@"TripReport%@TO%@.csv",[dateFormatter stringFromDate:fromDate],[dateFormatter stringFromDate:toDate]];
                [mailViewController addAttachmentData:myData mimeType:@"text/csv" fileName:fileane];
            }
            
            [self presentViewController:mailViewController animated:YES completion:nil];
            
        }
        else {
            [self deleteCreatedFiles];
            [SharedMethods showMessage:@"Error" message:@"Please check your e-mail configurations! (email app)" onVC:self];
        }

        }
    @catch (NSException * e) {
        [SharedMethods showMessage:@"Error" message:@"Please try again" onVC:self];
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    [self deleteCreatedFiles];
}

-(void)deleteCreatedFiles{
    @try {
        NSString *fulpath1 = @"";
        NSString *fulpath2 = @"";
        
        NSString *filename1 = [NSString stringWithFormat:@"TripReport%@TO%@.pdf",[dateFormatter stringFromDate:fromDate],[dateFormatter stringFromDate:toDate]];
        NSString *filename2 = [NSString stringWithFormat:@"TripReport%@TO%@.csv",[dateFormatter stringFromDate:fromDate],[dateFormatter stringFromDate:toDate]];
        
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        
        fulpath1 = [documentsDir stringByAppendingPathComponent:filename1];
        fulpath2 = [documentsDir stringByAppendingPathComponent:filename2];
        
        NSFileManager *fm = [NSFileManager defaultManager];
        
        NSError *error=nil;
        
        if ([fm fileExistsAtPath:fulpath1]) {
            [fm removeItemAtPath:fulpath1 error:&error];
        }
        
        if ([fm fileExistsAtPath:fulpath2]) {
            [fm removeItemAtPath:fulpath2 error:&error];
        }
    }
    @catch (NSException * e) {
        
    }
}


@end
