//
//  SettingsVC.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsDataStore.h"
#import "SettingsController.h"
#import "LoginController.h"
#import "SharedMethods.h"
#import "LoginViewController.h"

@interface SettingsVC()<UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource, SettingsUIDelagate, UITextFieldDelegate, LoginUIDelagate> {
    __weak IBOutlet UITextField *txtCar;
    __weak IBOutlet UIButton *btnLogOut;
    __weak IBOutlet UIButton *btnSave;
    __weak IBOutlet UILabel *lblVersion;
    __weak IBOutlet UIPickerView *pckTrackingType;
    __weak IBOutlet UITableView *carTable;

    int selectedTracking;
    int selectedCarId;
    
    SettingsController *settingsController;
    LoginController *loginController;
    
    NSMutableArray *carList;
    
    UIView *topOverlay;
    
    UIAlertAction *done;
}

@end

@implementation SettingsVC

-(void)viewDidLoad{
    [super viewDidLoad];
    
    settingsController = [[SettingsController alloc] init];
    settingsController.uiSettingsDelagate = self;
    loginController = [[LoginController alloc] init];
    loginController.uiLoginDelagate = self;
    
    [self refreshCarTable];
}

-(void)refreshCarTable{
    carTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    carList = [settingsController getCarList];
    [carTable reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    selectedCarId = [SettingsDataStore getSharedInstance].car.carId;
    txtCar.text = [SettingsDataStore getSharedInstance].car.carName;
    selectedTracking = [SettingsDataStore getSharedInstance].user.trackingType;
    [pckTrackingType reloadAllComponents];
    [pckTrackingType selectRow:selectedTracking-1 inComponent:0 animated:YES];
    
    NSString *buildNumber = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    lblVersion.text = [NSString stringWithFormat:@"iMileage Version %@",buildNumber];
}

- (IBAction)saveBtnTapped:(id)sender {
    User *user  = [[SettingsDataStore getSharedInstance].user copy];
    user.selectedCarId = selectedCarId;
    user.trackingType = selectedTracking;
    
    [loginController updateAccount:user];
}

- (IBAction)logOutBtnTapped:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:0] forKey:@"userId"];
    [defaults synchronize];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:nil];
    LoginViewController *vc = [sb instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)carFieldTapped:(UIButton*)sender {
    topOverlay = [[UIView alloc] init];
    topOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    topOverlay.frame = self.view.frame;
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [topOverlay addGestureRecognizer:singleFingerTap];
    
    CGRect txtFrame = sender.frame;
    CGRect rowFrame = [carTable rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    int height = rowFrame.size.height * 6;
    carTable.frame = CGRectMake((self.view.frame.size.width/2)-(carTable.frame.size.width/2), txtFrame.origin.y + txtFrame.size.height + 10, carTable.frame.size.width, height);
    
    [self.view addSubview:topOverlay];
    [self.view addSubview:carTable];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer{
    [carTable removeFromSuperview];
    [topOverlay removeFromSuperview];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    if(row == 0)
        title = @"GPS Tracking";
    else
        title = @"Manual Tracking";
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    return attString;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectedTracking = (int)row + 1;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [carList count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSString *title;
    if(indexPath.row == [carList count])
        title = @"Add New Car";
    else{
        Car *car = [carList objectAtIndex:indexPath.row];
        title = car.carName;
    }
    cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.text = title;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self handleSingleTap:nil];
    if(indexPath.row == [carList count]){
        [self openAddCar];
    }
    else{
        Car *car = [carList objectAtIndex:indexPath.row];
        selectedCarId = car.carId;
        txtCar.text = car.carName;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == [carList count])
        return NO;
    else
        return YES;
}

- (void)tableView:(UITableView*)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath{
    Car *car = [carList objectAtIndex:indexPath.row];
    if(car.carId == [SettingsDataStore getSharedInstance].user.selectedCarId){
        [SharedMethods showMessage:@"Error" message:@"You cannot delete the Selected Car" onVC:self];
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Car" message:@"Are you sure you want to delete this Car?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yes = [UIAlertAction
                          actionWithTitle:@"Yes"
                          style:UIAlertActionStyleDestructive
                          handler:^(UIAlertAction * action)
                          {
                              [settingsController deleteCar:car.carId];
                          }];
    [alert addAction:yes];
    
    UIAlertAction *no = [UIAlertAction
                         actionWithTitle:@"No"
                         style:UIAlertActionStyleCancel
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    [alert addAction:no];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)openAddCar{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add New Car" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Car Name";
        textField.delegate = self;
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    }];
    
    done = [UIAlertAction
                           actionWithTitle:@"Add"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               if(![[alert textFields][0].text isEqualToString:@""]){
                                   Car *car = [[Car alloc] init];
                                   car.carName = [alert textFields][0].text;
                                   
                                   [settingsController addCar:car];
                               }
                    
                           }];
    [done setEnabled:NO];
    [alert addAction:done];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(finalString.length > 0){
        [done setEnabled:YES];
    }
    else{
        [done setEnabled:NO];
    }
    return YES;
}

-(void)addCarSuccess:(Car*)car{
    selectedCarId = car.carId;
    [self refreshCarTable];
    txtCar.text = car.carName;
}

-(void)addCarFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

-(void)updateSuccess{
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"carId == %d",selectedCarId];
    NSArray *result = [carList filteredArrayUsingPredicate:filter];
    [SettingsDataStore getSharedInstance].car = result[0] ? ((Car*)result[0]) : [SettingsDataStore getSharedInstance].car;
    [SharedMethods showMessage:@"Message" message:@"Settings Saved Successfully" onVC:self];
}

-(void)updateFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

-(void)deleteCarSuccess{
    [self refreshCarTable];
}

-(void)deleteCarFailure:(NSString*)msg{
    [SharedMethods showMessage:@"Error" message:msg onVC:self];
}

@end
