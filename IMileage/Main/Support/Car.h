//
//  Car.h
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Car : NSObject

@property (nonatomic, assign) int carId;
@property (nonatomic, strong) NSString *carName;
@property (nonatomic, assign) double mileage;

@end
