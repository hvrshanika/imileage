//
//  Trip.h
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#define BUSINESS_TRIP 1
#define PERSONAL_TRIP 2

#define GPS_TRACKING 1
#define MANUAL_TRACKING 2

#import <Foundation/Foundation.h>

@interface Trip : NSObject

@property (nonatomic, assign) int tripId;
@property (nonatomic, assign) int tripType;
@property (nonatomic, assign) int trakingType;
@property (nonatomic, assign) double distance;
@property (nonatomic, assign) double tripTime;
@property (nonatomic, strong) NSString *startLocation;
@property (nonatomic, strong) NSString *endLocation;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, assign) int carId;
@property (nonatomic, assign) int userId;
@property (nonatomic, strong) NSString *startedDateTime;
@property (nonatomic, strong) NSString *endDateTime;

@end
