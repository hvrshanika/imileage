//
//  User.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/22/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "User.h"

@implementation User

- (id)copyWithZone:(nullable NSZone *)zone{
    User *user = [[User alloc] init];
    
    user.userId = self.userId;
    user.email = [self.email copy];
    user.name = [self.name copy];
    user.selectedCarId = self.selectedCarId;
    user.trackingType = self.trackingType;
    
    return user;
}

@end
