//
//  PDFItem.h
//  OctoFoodPOS
//
//  Created by Rajeev Prasad on 6/11/14.
//
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    PDFItemTypeNormal = 0,
    PDFItemTypeBundle,
    PDFItemTypeMod,
} PDFItemType;

@interface PDFItem : NSObject

@property (nonatomic, assign) int itemId;
@property (nonatomic, strong) NSString *descriptionName;
@property (nonatomic, strong) NSString *descriptionCode;
@property (nonatomic, assign) int qty;
@property (nonatomic, assign) double price;
@property (nonatomic, assign) double discount;
@property (nonatomic, assign) double total;
@property (nonatomic, assign) double taxPct;
@property (nonatomic, strong) NSString *taxCode;

@property (nonatomic, assign) PDFItemType type;
@property (nonatomic, assign) int typeParent;
@property (nonatomic, strong) NSMutableArray *subItems;
@property (nonatomic, strong) NSMutableArray *serialArray;

// Added 12 February 2017 - Tax Code Enhancements for Food POS
@property (nonatomic, assign) double taxAmount;
@property (nonatomic, assign) double finalItemWRSPDiscount;
@property (nonatomic, assign) double serviceCharge;
@property (nonatomic, assign) BOOL isTaxable;

// Added 30 March 2017 - Tax Summary fix when Tax Inclusive
@property (nonatomic, assign) BOOL isTaxInclusive;

// Added 20 August 2017 - Service Charge
@property (nonatomic, assign) BOOL isServiceChargeInclusive;
@property (nonatomic, assign) double serviceChargeAmount;
@property (nonatomic, assign) double serviceChargeTax;

-(id)initWithId:(int)oId name:(NSString *)oName qty:(int)oQty price:(double)oPrice discount:(double)oDiscount total:(double)oTotal;
-(id)initWithId:(int)oId name:(NSString *)oName qty:(int)oQty price:(double)oPrice discount:(double)oDiscount total:(double)oTotal andTaxPct:(double)tax;
-(id)initWithId:(int)oId name:(NSString *)oName code:(NSString *)oCode qty:(int)oQty price:(double)oPrice discount:(double)oDiscount total:(double)oTotal andTaxPct:(double)tax;
-(id)initWithId:(int)oId name:(NSString *)oName qty:(int)oQty price:(double)oPrice discount:(double)oDiscount total:(double)oTotal andTaxPct:(double)tax serialArray:(NSMutableArray *)sArray;
-(id)initWithId:(int)oId name:(NSString *)oName total:(double)oTotal;

@end
