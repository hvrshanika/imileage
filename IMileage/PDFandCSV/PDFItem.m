//
//  PDFItem.m
//  OctoFoodPOS
//
//  Created by Rajeev Prasad on 6/11/14.
//
//

#import "PDFItem.h"

@implementation PDFItem

-(id)initWithId:(int)oId name:(NSString *)oName qty:(int)oQty price:(double)oPrice discount:(double)oDiscount total:(double)oTotal andTaxPct:(double)tax{
    self = [super init];
    self.itemId = oId;
    self.descriptionName = oName;
    self.qty = oQty;
    self.price = oPrice;
    self.discount = oDiscount;
    self.total = oTotal;
    self.type = PDFItemTypeNormal;
    self.typeParent = 0;
    self.taxPct = tax;
    
    self.taxAmount = -1;
    self.serviceCharge = 0;
    
    return self;
}

-(id)initWithId:(int)oId name:(NSString *)oName code:(NSString *)oCode qty:(int)oQty price:(double)oPrice discount:(double)oDiscount total:(double)oTotal andTaxPct:(double)tax{
    self = [super init];
    self.itemId = oId;
    self.descriptionName = oName;
    self.descriptionCode = oCode;
    self.qty = oQty;
    self.price = oPrice;
    self.discount = oDiscount;
    self.total = oTotal;
    self.type = PDFItemTypeNormal;
    self.typeParent = 0;
    self.taxPct = tax;
    
    self.taxAmount = -1;
    self.serviceCharge = 0;
    
    return self;
}

-(id)initWithId:(int)oId name:(NSString *)oName qty:(int)oQty price:(double)oPrice discount:(double)oDiscount total:(double)oTotal{
    return [self initWithId:oId name:oName qty:oQty price:oPrice discount:oDiscount total:oTotal andTaxPct:0];
}

-(id)initWithId:(int)oId name:(NSString *)oName total:(double)oTotal{
    self = [super init];
    self.itemId = oId;
    self.descriptionName = oName;
    self.total = oTotal;
    
    self.taxAmount = -1;
    self.serviceCharge = 0;
    
    return self;
}

-(id)initWithId:(int)oId name:(NSString *)oName qty:(int)oQty price:(double)oPrice discount:(double)oDiscount total:(double)oTotal andTaxPct:(double)tax serialArray:(NSMutableArray *)sArray{
    self = [super init];
    self.itemId = oId;
    self.descriptionName = oName;
    self.qty = oQty;
    self.price = oPrice;
    self.discount = oDiscount;
    self.total = oTotal;
    self.type = PDFItemTypeNormal;
    self.typeParent = 0;
    self.taxPct = tax;
    self.serialArray = sArray;
    
    self.taxAmount = -1;
    self.serviceCharge = 0;
    
    return self;
}

@end
