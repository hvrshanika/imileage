//
//  PDFWriter.h
//  FoodPOS
//
//  Created by Rajeev Prasad on 29/1/13.
//
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FileOrientation){
    FileOrientationPortrait,
    FileOrientationLandscape
};

@interface PDFWriter : NSObject

-(id)initWithFileName:(NSString *)name;
-(id)initWithFileName:(NSString *)name withOrientation:(FileOrientation) orientation;

-(void)drawTitle:(NSString *)stringg centerAlign:(BOOL)center percentageOffser:(int)percentageOffset;
-(void)drawHeader:(NSString *)string centerAlign:(BOOL)center percentageOffser:(int)percentageOffset;

-(void)drawEmptyLine;
-(void)startNewPage;

-(void)drawDataTable:(NSMutableArray *)tableData withColumnHeaders:(NSMutableArray *)headers withColumnPercentageWidths:(NSMutableArray *)colWidths withColumnAlignments:(NSMutableArray *)colAlign;

-(void)drawDataTable:(NSMutableArray *)tableData withColumnHeaders:(NSMutableArray *)headers withColumnPercentageWidths:(NSMutableArray *)colWidths withColumnAlignments:(NSMutableArray *)colAlign boldLastRow:(BOOL)boldLast;

-(void)drawDataTableDict:(NSMutableDictionary *)tableData withColumnHeaders:(NSMutableArray *)headers withColumnPercentageWidths:(NSMutableArray *)colWidths withColumnAlignments:(NSMutableArray *)colAlign boldFirstRow:(BOOL)boldFR;

-(void)drawSingleTableRow:(NSMutableArray *)rowData Bold:(BOOL)bold;
-(void)finishPDFFile;

@end
