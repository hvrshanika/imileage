//
//  PDFWriter.m
//  FoodPOS
//
//  Created by Rajeev Prasad on 29/1/13.
//
//

#import "PDFWriter.h"
#import <UIKit/UIKit.h>

#define LINES_PER_PAGE 43
#define ITEM_HEIGHT 18

@interface PDFWriter(){
    CGRect pageRect;
    CGContextRef pdfContext;
    int currentLine;
    int pageNumber;
    NSMutableArray *columnHeaders;
    NSMutableArray *columnXCords;
    NSMutableArray *columnWidths;
    NSMutableArray *columnAlignment;
}
@property (nonatomic, assign) CGContextRef pdfContext;

@property (nonatomic, strong) NSMutableArray *columnHeaders;
@property (nonatomic, strong) NSMutableArray *columnXCords;
@property (nonatomic, strong) NSMutableArray *columnWidths;
@property (nonatomic, strong) NSMutableArray *columnAlignment;

@property (nonatomic, assign) FileOrientation orientation;

@property (nonatomic, assign) double contentWidth;
@property (nonatomic, assign) double contentHeight;
@property (nonatomic, assign) double currentHeight;
@property (nonatomic, assign) BOOL shouldStartNewPage;

@end

@implementation PDFWriter
@synthesize pdfContext;
@synthesize columnHeaders,columnAlignment, columnWidths, columnXCords;

-(id)initWithFileName:(NSString *)name{
    self = [super init];
    self.orientation = FileOrientationPortrait;
    pageRect = CGRectMake(0, 0, 595, 860);
    
    self.contentWidth = 475;
    self.contentHeight = 800;
    self.currentHeight = 0;
    self.shouldStartNewPage = NO;
    
    UIGraphicsBeginPDFContextToFile(name, CGRectZero, nil);
    self.pdfContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(pdfContext, 0.0, 0.0, 0.0, 1.0);
    
    pageNumber = 1;
    currentLine = 1;
    UIGraphicsBeginPDFPageWithInfo(pageRect, nil);
    [self drawPageNumber];
    
    return self;
}

-(id)initWithFileName:(NSString *)name withOrientation:(FileOrientation) orientation{
    self = [super init];
    self.orientation = orientation;
    pageRect = orientation == FileOrientationPortrait ? CGRectMake(0, 0, 595, 860) : CGRectMake(0, 0, 860, 595);
    
    self.contentWidth = orientation == FileOrientationPortrait ? 475 : 740;
    self.contentHeight = orientation == FileOrientationPortrait ? 800 : 530;
    self.currentHeight = 0;
    self.shouldStartNewPage = NO;
    
    UIGraphicsBeginPDFContextToFile(name, CGRectZero, nil);
    self.pdfContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(pdfContext, 0.0, 0.0, 0.0, 1.0);
    
    pageNumber = 1;
    currentLine = 1;
    UIGraphicsBeginPDFPageWithInfo(pageRect, nil);
    [self drawPageNumber];
    
    return self;
}

-(void)drawTitle:(NSString *)string centerAlign:(BOOL)center percentageOffser:(int)percentageOffset{
   
    [self checkContentHeightLimit];
    
    UIFont *headerFont= [UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:20];
    
    int pageLine = currentLine % LINES_PER_PAGE;
    int y = 20 + (pageLine * ITEM_HEIGHT);
    
    int offset = (self.contentWidth * percentageOffset / 100);
    
    if(center){
        [self drawText:string inRect:CGRectMake(60+offset, y,self.contentWidth, 30) font:headerFont andAlignment:NSTextAlignmentCenter];
    } else {
        [self drawText:string inRect:CGRectMake(60+offset, y,self.contentWidth, 30) font:headerFont andAlignment:NSTextAlignmentLeft];
    }
    
    currentLine += 1;
    
    self.currentHeight += 30;
}

-(void)drawHeader:(NSString *)string centerAlign:(BOOL)center percentageOffser:(int)percentageOffset{
    
    [self checkContentHeightLimit];
    
    UIFont *subheaderFont= [UIFont fontWithName:@"TimesNewRomanPSMT" size:14];
    
    int pageLine = currentLine % LINES_PER_PAGE;
    int y = 30 + (pageLine * ITEM_HEIGHT);
    
    int offset = (self.contentWidth * percentageOffset / 100);
    
    if(center){
        [self drawText:string inRect:CGRectMake(60+offset, y,self.contentWidth, 20) font:subheaderFont andAlignment:NSTextAlignmentCenter];
    } else {
        [self drawText:string inRect:CGRectMake(60+offset, y,self.contentWidth, 20) font:subheaderFont andAlignment:NSTextAlignmentLeft];
    }
    
    currentLine++;
    
    self.currentHeight += 30;
}

-(void)drawEmptyLine{
    if((currentLine % LINES_PER_PAGE) == 0){ // end of page
        pageNumber++;
        UIGraphicsBeginPDFPageWithInfo(pageRect, nil);
        [self drawPageNumber];
        currentLine++;
    }
    currentLine++;
}

-(void)startNewPage{
    int x = currentLine / LINES_PER_PAGE;
    currentLine = (x+1)*LINES_PER_PAGE;
    if((currentLine % LINES_PER_PAGE) == 0){ // end of page
        pageNumber++;
        UIGraphicsBeginPDFPageWithInfo(pageRect, nil);
        [self drawPageNumber];
        currentLine++;
        
        self.currentHeight = 0;
        self.shouldStartNewPage = NO;
    }
}

-(void)drawDataTable:(NSMutableArray *)tableData withColumnHeaders:(NSMutableArray *)headers withColumnPercentageWidths:(NSMutableArray *)colWidths withColumnAlignments:(NSMutableArray *)colAlign{
    [self drawDataTable:tableData withColumnHeaders:headers withColumnPercentageWidths:colWidths withColumnAlignments:colAlign boldLastRow:NO];
}

-(void)drawDataTable:(NSMutableArray *)tableData withColumnHeaders:(NSMutableArray *)headers withColumnPercentageWidths:(NSMutableArray *)colWidths withColumnAlignments:(NSMutableArray *)colAlign boldLastRow:(BOOL)boldLast{
    
    [self checkContentHeightLimit];
    
    self.columnHeaders = headers;
    self.columnAlignment = colAlign;
    self.columnXCords = [NSMutableArray array];
    self.columnWidths = [NSMutableArray array];
    
    int curX = 60;
    [columnXCords addObject:@(curX)];
    for(NSNumber *w in colWidths){
        double perW = [w doubleValue];
        double width = (self.contentWidth * perW / 100);
        [columnWidths addObject:@(width)];
        curX += width;
        [columnXCords addObject:@(curX)];
    }
    
    [self drawTableHeader:columnHeaders];
    
    if(!boldLast){
        for( NSMutableArray *array in tableData){
            [self drawTableRow:array];
        }
    } else {
        int count = (int)[tableData count];
        for(int i=0; i<count; i++){
            NSMutableArray *array = tableData[i];
            if(i < (count-1)){
                [self drawTableRow:array];
            } else {
                [self drawSingleTableRow:array Bold:YES];
            }
        }
    }
}

-(void)drawDataTableDict:(NSMutableDictionary *)tableData withColumnHeaders:(NSMutableArray *)headers withColumnPercentageWidths:(NSMutableArray *)colWidths withColumnAlignments:(NSMutableArray *)colAlign boldFirstRow:(BOOL)boldFR{
    
    [self checkContentHeightLimit];
    
    self.columnHeaders = headers;
    self.columnAlignment = colAlign;
    self.columnXCords = [NSMutableArray array];
    self.columnWidths = [NSMutableArray array];
    
    int curX = 60;
    [columnXCords addObject:[NSNumber numberWithInt:curX]];
    for(NSNumber *w in colWidths){
        int perW = [w intValue];
        int width = (self.contentWidth * perW / 100);
        [columnWidths addObject:[NSNumber numberWithInt:width]];
        curX += width;
        [columnXCords addObject:[NSNumber numberWithInt:curX]];
    }
    
    [self drawTableHeader:columnHeaders];
    
    int count = (int)[tableData count];
    
    for(int i=0; i<count; i++){
        NSMutableArray *array = [tableData objectForKey:[NSNumber numberWithInt:i]];
        int line = 0;
        for(NSMutableArray *lineData in array){
            if(line == 0 && boldFR){
                [self drawSingleTableRow:lineData Bold:YES];
            } else {
                [self drawTableRow:lineData];
            }
            line++;
        }
    }
}

-(void)drawTableHeader:(NSMutableArray *)headerData{
    
     [self checkContentHeightLimit];
    
    UIFont *valueFont= [UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:10];
    
    int pageLine = currentLine % LINES_PER_PAGE;
    
    int i = 0;
    for(NSString *value in headerData){
        int al = [columnAlignment[i] intValue];
        int x = [columnXCords[i] intValue];
        int y = 30 + (pageLine * ITEM_HEIGHT);
        
        [self drawText:value inRect:CGRectMake(x, y,[columnWidths[i] intValue], 20) font:valueFont andAlignment:al];
        i++;
    }
    if (i > 0) {
        currentLine++;
        self.currentHeight += 20;
    }
}

-(void)drawTableRow:(NSMutableArray *)rowData{

    [self checkContentHeightLimit];
    
    if((currentLine % LINES_PER_PAGE) == 0){ // end of page
        pageNumber++;
        UIGraphicsBeginPDFPageWithInfo(pageRect, nil);
        [self drawPageNumber];
        currentLine++;
        [self drawTableHeader:columnHeaders];
    } else if(rowData ==nil || [rowData count]==0){
        [self drawEmptyLine];
        return;
    }
    
    UIFont *valueFont= [UIFont fontWithName:@"TimesNewRomanPSMT" size:10];
    
    int pageLine = currentLine % LINES_PER_PAGE;
    
    int i = 0;
    for(NSString *value in rowData){
        int al = [columnAlignment[i] intValue];
        int x = [columnXCords[i] intValue];
        int y = 30 + (pageLine * ITEM_HEIGHT);
        
        [self drawText:value inRect:CGRectMake(x, y,[columnWidths[i] intValue], 20) font:valueFont andAlignment:al];
        i++;
    }
    currentLine++;
    
    self.currentHeight += 20;
}

-(void)drawSingleTableRow:(NSMutableArray *)rowData Bold:(BOOL)bold{
    
    [self checkContentHeightLimit];
    
    if((currentLine % LINES_PER_PAGE) == 0){ // end of page
        pageNumber++;
        UIGraphicsBeginPDFPageWithInfo(pageRect, nil);
        [self drawPageNumber];
        currentLine++;
    }else if(rowData ==nil || [rowData count]==0){
        [self drawEmptyLine];
        return;
    }
    
    UIFont *valueFont = nil;
    if(!bold)
        valueFont= [UIFont fontWithName:@"TimesNewRomanPSMT" size:10];
    else
        valueFont= [UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:10];
    
    int pageLine = currentLine % LINES_PER_PAGE;
    
    int i = 0;
    for(NSString *value in rowData){
        int al = [columnAlignment[i] intValue];
        int x = [columnXCords[i] intValue];
        int y = 30 + (pageLine * ITEM_HEIGHT);
        
        [self drawText:value inRect:CGRectMake(x, y,[columnWidths[i] intValue], 20) font:valueFont andAlignment:al];
        i++;
    }
    currentLine++;
    
    self.currentHeight += 20;
}

-(void)drawPageNumber{
    UIFont *font= [UIFont fontWithName:@"TimesNewRomanPSMT" size:10];
    
    int x = 440;
    int y = 820;
    
    if(self.orientation == FileOrientationLandscape){
        x = 730;
        y = 565;
    }

    NSString *pageNo = [NSString stringWithFormat:@"- Page %02d -", pageNumber];
    
    [self drawText:pageNo inRect:CGRectMake(x, y, 100, 20) font:font andAlignment:NSTextAlignmentRight];
}

-(void)finishPDFFile{
    UIGraphicsEndPDFContext();
}

// Modified 18 January 2017 - Resize text that is overflowing from rect boundary.
- (void) drawText:(NSString *)string inRect:(CGRect)rect font:(UIFont*)font andAlignment:(NSTextAlignment)align{
    
    UIFont *definedFont = font;
    CGFloat fontSize = definedFont.pointSize;
    
    CGSize sampleSize = [string sizeWithAttributes:@{ NSFontAttributeName: font }];
    CGFloat ratio = rect.size.width / sampleSize.width;
    
    if(ratio < 1)
        fontSize *= (ratio - 0.05); // Reduce it slightly more, to create spacing.
    
    UIFont *resizedfont = [font fontWithSize: fontSize];
    
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.0, 0.0, 0.0, 1.0);
    
    NSMutableParagraphStyle *textAlign = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [textAlign setAlignment:align];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys: resizedfont, NSFontAttributeName, textAlign, NSParagraphStyleAttributeName, nil];
    [string drawInRect:rect withAttributes:attributes];
}

// Added 18 January 2017 - Document Content Height Limit
- (void) checkContentHeightLimit{
    
    if(self.shouldStartNewPage){
        
        [self drawPageNumber];
        [self startNewPage];
        
    }else if(self.currentHeight >= self.contentHeight){
        
        self.shouldStartNewPage = YES;
    }
}

@end
