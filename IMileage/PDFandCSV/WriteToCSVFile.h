//
//  WriteToCSVFile.h
//  FoodPos
//
//  Created by Rajeev  Prasad on 2/15/12.
//  Copyright (c) 2012 Eleos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSVWriter.h"

@interface WriteToCSVFile : NSObject{
    
}

+(void)writeToCSVFile:(NSMutableArray *)dataArray FilePath:(NSString *)csvFile;

@end
