//
//  WriteToCSVFile.m
//  FoodPos
//
//  Created by Rajeev  Prasad on 2/15/12.
//  Copyright (c) 2012 Eleos. All rights reserved.
//

#import "WriteToCSVFile.h"

@implementation WriteToCSVFile

+(void)writeToCSVFile:(NSMutableArray *)dataArray FilePath:(NSString *)csvFile{
        
    BOOL atomically = YES;
    NSString *delimiter = @",";
    
    CSVWriter * writer = [[CSVWriter alloc] initWithCSVFile:csvFile atomic:atomically];
	[writer setDelimiter:delimiter];
	for (NSMutableArray *row in dataArray) {
		[writer writeLineWithFields:row];
	}
	
    NSError *error = nil;
    
	BOOL ok = ([writer error] == nil);
	if (!ok) {
		error = [writer error];
        NSLog(@"Error! - %@", [error description]);
	}
	[writer closeFile];
}

@end
