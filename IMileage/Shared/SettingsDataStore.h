//
//  SettingsDataStore.h
//  IMileage
//
//  Created by Rajith Vithanage on 3/9/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Car.h"

//#define DEBUG_AUTO_LOGIN @"DEBUG_AUTO_LOGIN"
//#define DEBUG_AUTO_LOGIN_USERNAME @"shanika@gmail.com"
//#define DEBUG_AUTO_LOGIN_PASSWORD @"1234"

@interface SettingsDataStore : NSObject

+(SettingsDataStore*)getSharedInstance;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) Car *car;

-(NSDateFormatter*)getTripDateTimeFormatter;

@end
