//
//  SettingsDataStore.m
//  IMileage
//
//  Created by Rajith Vithanage on 3/9/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "SettingsDataStore.h"

static SettingsDataStore *shared;

@interface SettingsDataStore(){
    NSDateFormatter *tripDateTimeFormatter;
}

@end

@implementation SettingsDataStore

+(SettingsDataStore*)getSharedInstance{
    @synchronized(self){
        if (shared==NULL) {
            shared = [[SettingsDataStore alloc] init];
        }
    }
    return shared;
}

-(id) init{
    self = [super init];
    tripDateTimeFormatter = [[NSDateFormatter alloc] init];
    [tripDateTimeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return self;
}

-(NSDateFormatter*)getTripDateTimeFormatter{
    return tripDateTimeFormatter;
}

@end
