//
//  SharedMethods.h
//  IMileage
//
//  Created by Rajith Vithanage on 3/9/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SharedMethods : NSObject

+(void)showMessage:(NSString*)title message:(NSString*)msg onVC:(UIViewController*)vc;

@end
