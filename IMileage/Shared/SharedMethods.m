//
//  SharedMethods.m
//  IMileage
//
//  Created by Rajith Vithanage on 3/9/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import "SharedMethods.h"

@implementation SharedMethods

+(void)showMessage:(NSString*)title message:(NSString*)msg onVC:(UIViewController*)vc{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:cancel];
    
    [vc presentViewController:alert animated:YES completion:nil];
}

@end
