//
//  main.m
//  IMileage
//
//  Created by Rajith Vithanage on 2/18/18.
//  Copyright © 2018 Rajith Vithanage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
